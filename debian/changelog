spread-sheet-widget (0.10-4) unstable; urgency=medium

  * Debian: Add source option to allow multiple builds without
    starting from clean sources.
    Closes: #1049603

 -- Friedrich Beckmann <friedrich.beckmann@gmx.de>  Mon, 08 Jan 2024 14:49:16 +0100

spread-sheet-widget (0.10-3) unstable; urgency=medium

  * Removed ChangeLog from being distributed (closes: #1059923)

 -- Friedrich Beckmann <friedrich.beckmann@gmx.de>  Wed, 03 Jan 2024 22:39:46 +0100

spread-sheet-widget (0.10-2) unstable; urgency=medium

  * Fixed debian build artefact (closes: #1059923)

 -- Friedrich Beckmann <friedrich.beckmann@gmx.de>  Wed, 03 Jan 2024 19:02:39 +0100

spread-sheet-widget (0.10-1) unstable; urgency=medium

  * New upstream release 0.10
  * Fix RTL selection rendering
  * debian: add texinfo dependency for makeinfo

 -- Friedrich Beckmann <friedrich.beckmann@gmx.de>  Wed, 03 Jan 2024 16:21:57 +0100

spread-sheet-widget (0.8-1) unstable; urgency=medium

  * New upstream release 0.8
  * many bugfixes
  * support for w32 builds
  * debian: Removed Ben as uploader (closes: #995788)

 -- Friedrich Beckmann <friedrich.beckmann@gmx.de>  Thu, 07 Apr 2022 15:10:38 +0200

spread-sheet-widget (0.6-3) unstable; urgency=medium

  * Revert previous change - was a build problem
    see: https://lists.gnu.org/archive/html/pspp-dev/2020-08/msg00030.html
  * moved info files to spread-sheet-widget-dev package

 -- Friedrich Beckmann <friedrich.beckmann@gmx.de>  Mon, 17 Aug 2020 15:58:18 +0200

spread-sheet-widget (0.6-2) unstable; urgency=medium

  * Removed Multi-Arch: same
    The info files are not architecture independent

 -- Friedrich Beckmann <friedrich.beckmann@gmx.de>  Fri, 14 Aug 2020 11:36:00 +0200

spread-sheet-widget (0.6-1) unstable; urgency=medium

  * New upstream release 0.6
  * bugfixes for memoryleaks and view updates
  * don't show emojis
  * fix copy/paste/cut - pspp #57274
  * install info file

 -- Friedrich Beckmann <friedrich.beckmann@gmx.de>  Thu, 13 Aug 2020 09:37:58 +0200

spread-sheet-widget (0.4-1) unstable; urgency=medium

  * New upstream release 0.4
  * several bug fixes and speed improvements
  * debian: changed compat to debhelper-compat
  * debian: add hardening
  * debian: remove lintian override
  * debian: update copyright
  * debian: upstream metadata add bugreport
  * debian: add Johns signing-key for upstream

 -- Friedrich Beckmann <friedrich.beckmann@gmx.de>  Sun, 21 Jun 2020 22:17:03 +0200

spread-sheet-widget (0.3-1) unstable; urgency=low

  * Initial release.
    Closes: #913118

 -- Friedrich Beckmann <friedrich.beckmann@gmx.de>  Mon, 29 Oct 2018 07:19:01 +0100

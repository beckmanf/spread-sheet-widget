This is spread-sheet-widget.info, produced by makeinfo version 6.8 from
spread-sheet-widget.texi.

This manual is for GNU Spread Sheet Widget version 0.10.

   Copyright (C) 2017, 2020 John Darrington

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU Free Documentation License,
     Version 1.3 or any later version published by the Free Software
     Foundation; with no Invariant Sections, no Front-Cover Texts, and
     no Back-Cover Texts.  A copy of the license is included in the
     section entitled "GNU Free Documentation License".
INFO-DIR-SECTION Development
START-INFO-DIR-ENTRY
* SpreadSheetWidget: (ssw).             A Spread Sheet Widget
END-INFO-DIR-ENTRY


File: spread-sheet-widget.info,  Node: Top,  Up: (dir)

GNU Spread Sheet Widget
***********************

This manual is for GNU Spread Sheet Widget version 0.10.

   Copyright (C) 2017, 2020 John Darrington

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU Free Documentation License,
     Version 1.3 or any later version published by the Free Software
     Foundation; with no Invariant Sections, no Front-Cover Texts, and
     no Back-Cover Texts.  A copy of the license is included in the
     section entitled "GNU Free Documentation License".

1 Introduction
**************

1.1 About this Manual
=====================

This manual documents the capabilities and the programming interface of
GNU Spread Sheet Widget (version 0.10).  It assumes that the reader is
already familiar with elementary computer usage, the C programming
language and with simple programming using Gtk+ version 3.

1.2 About Spread Sheet Widget
=============================

GNU Spread Sheet Widget is a library providing a Gtk+ widget which can
be used by applications to present numerical and alpha-numerical data in
a rectangular grid similar to that used by modern spread sheet
applications.  It also provides a user interface to facilitate the
creation of new data and the manipulation and deletion of existing data
by the application's user.

   Spread Sheet Widget aspires to be as fast as possible and it avoids
allocating memory except when necessary.  This makes it suitable for use
with very large amounts of data.

   Spread Sheet Widget is not a spread sheet application.  It does not
provide any facilities for arithmetic or other mathematical operations
on the data which it displays.

2 Programming with Spread Sheet Widget.
***************************************

2.1 Installing the Spread Sheet Widget Library
==============================================

Clearly, before one can start programming with the library it has to be
installed.

   If your operating system has already installed Spread Sheet Widget,
then you need not bother installing it yourself.  Note however that many
popular operating systems provide separate "runtime" and "development"
packages; the development package often denoted with the prefix/suffix
"-dev" or "-devel".  In order to write code using the library one needs
the development version.

   Otherwise, you may install the library as you would with any other:
     ./configure
     make
     # become root
     make install
This process is described in excruciating detail in 'INSTALL' and will
not be further discussed here.

2.2 Getting Started
===================

A simple program using Spread Sheet Widget is as follows:
          1	#include <gtk/gtk.h>
          2	#include <ssw-sheet.h>
          3	
          4	int
          5	main (int argc, char **argv)
          6	{
          7	  GtkWidget *window;
          8	  GtkWidget *sheet;
          9	  gtk_init (&argc, &argv);
         10	
         11	  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
         12	
         13	  sheet = ssw_sheet_new ();
         14	
         15	  gtk_container_add (GTK_CONTAINER (window), sheet);
         16	  
         17	  gtk_window_maximize (GTK_WINDOW (window));
         18	
         19	  gtk_widget_show_all (window);
         20	
         21	  gtk_main ();
         22	
         23	  return 0;
         24	}
On a standard GNU/Linux system you can compile this program with a
command such as 'gcc $(pkg-config --libs --cflags spread-sheet-widget)
prog1.c'.

   Except for lines 2 and 13, all code in this program are standard
Gtk+.  Line 2 includes the header file 'ssw-sheet.h' which is necessary
in order to call 'ssw_sheet_new' at line 13.  The function
'ssw_sheet_new' creates a new widget which is a Spread Sheet Widget to
be displayed.  Subsequent lines add the widget to the window and display
it.

   If you compile and run the program you will see an empty sheet
without any data in its cells.  As written, this program cannot display
any data because there is no "data model" associated with the
'SswSheet'.  The data model is a 'GObject' which implements the
'GtkTreeModel' interface.  One simple implementation of 'GtkTreeModel'
is the 'GtkListStore' object and the following example creates such an
object and sets it as the data model for our sheet:
          1	#include <gtk/gtk.h>
          2	#include <ssw-sheet.h>
          3	
          4	GtkTreeModel *
          5	create_data_model (void)
          6	{
          7	  GtkListStore *model = gtk_list_store_new (3,
          8						    G_TYPE_INT,
          9						    G_TYPE_STRING,
         10						    G_TYPE_DOUBLE);
         11	  for (int i = 0; i < 4; ++i)
         12	    {
         13	      GtkTreeIter iter;
         14	      gtk_list_store_append (model, &iter);
         15	      gtk_list_store_set (model, &iter,
         16				  0, i,
         17				  1, (i % 2) ? "odd" : "even",
         18				  2, i / 3.0,
         19				  -1);
         20	    }
         21	
         22	  return GTK_TREE_MODEL (model);
         23	}
         24	
         25	int
         26	main (int argc, char **argv)
         27	{
         28	  gtk_init (&argc, &argv);
         29	
         30	  GtkWidget *window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
         31	  GtkWidget *sheet = ssw_sheet_new ();
         32	  
         33	  GtkTreeModel *data = create_data_model ();
         34	  g_object_set (sheet, "data-model", data, NULL);
         35	  g_object_set (sheet, "editable", TRUE, NULL);
         36	
         37	  gtk_container_add (GTK_CONTAINER (window), sheet);
         38	  gtk_window_maximize (GTK_WINDOW (window));
         39	  gtk_widget_show_all (window);
         40	  gtk_main ();
         41	
         42	  return 0;
         43	}
   In this example, the function 'create_data_model' creates a model
with 3 columns and 4 rows.  'GtkListStore' is designed to store all its
data in memory.  This is fine for small data models, but would not be
suitable if there are a very large number of rows or columns.  In that
case you would be well advised not to use 'GtkListStore' but to create
your own 'GObject' implementing 'GtkTreeModel' and storing its data in
secondary memory.

   You inform the sheet about the data model to use, by way of the
'data-model' property of 'SswSheet'.  This is done in line 34 of the
above example.

   If you run the above example, you should see a sheet with 3 columns
of data and 4 rows.  Each datum is in a "cell".  Using the computer's
arrow keys you can select any cell.  To indicate that a cell is
selected, its border is highlighted.  However you will not be able to
edit any data, nor add any new data.  If you try to type anything your
keystrokes will be ignored.

   This is because the sheet's 'editable' property defaults to false.
In order to allow the user to add, delete or mutate data which the sheet
is displaying, you must set the 'editable' property to true.  The
typical way of doing this is 'g_object_set (sheet, "editable", TRUE,
NULL);'.  If you do this, and rerun the program, then you will be able
to type characters into a selected cell.  When you hit the <Enter>, --
_What?_  the cell returns to its previous value!!  What went wrong?

   This is because the spread sheet widget is designed to be independent
of how the data is stored.  It does not and cannot know how to update
data.  It is the programmer's responsibility to do this.



Tag Table:
Node: Top733

End Tag Table


Local Variables:
coding: utf-8
End:
